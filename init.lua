vim.g.mapleader = " "
vim.g.maplocalleader = ","

require("config.dep")
require("start")

-- Function to extract and display PNG images in the Markdown file
function show_images_from_md(file_path)
	local png_paths = {}

	-- Read the markdown file and extract paths to PNG files
	for line in io.lines(file_path) do
		for png_path in string.gmatch(line, "!%b[]%((.-%.png)%)") do
			table.insert(png_paths, png_path)
		end
	end

	-- Show each PNG file using wezterm's imgcat command
	for _, png in ipairs(png_paths) do
		vim.fn.jobstart("wezterm imgcat " .. png)
	end
end

-- Set an autocmd to run the function whenever you open a Markdown file
vim.api.nvim_create_autocmd("BufReadPost", {
	pattern = "*.md",
	callback = function()
		show_images_from_md(vim.fn.expand('%'))
	end,
	group = vim.api.nvim_create_augroup("DisplayPngInMd", { clear = true })
})

