return {
	"saghen/blink.cmp",
	lazy = false,
	dependencies = "rafamadriz/friendly-snippets",
	version = "v0.*",
	opts = {
		highlight = {
			use_nvim_cmp_as_default = true,
		},
		nerd_font_variant = "mono",
		accept = { auto_brackets = { enabled = true } },
		trigger = { signature_help = { enabled = true } },

		-- Custom snippets
		snippets = {
			bash = {
				{
					trig = "shebang",
					desc = "Bash shebang",
					snippet = "#!/bin/bash",
				},
				{
					trig = "forloop",
					desc = "For loop",
					snippet = "for ${1:var} in ${2:items}; do\n  ${3:echo $var}\ndone",
				},
				{
					trig = "if",
					desc = "If statement",
					snippet = "if [[ ${1:condition} ]]; then\n  ${2:# commands}\nfi",
				},
			},
			lua = {
				{
					trig = "func",
					desc = "Function definition",
					snippet = "function ${1:name}(${2:args})\n  ${3:-- body}\nend",
				},
				{
					trig = "fori",
					desc = "For loop (numeric)",
					snippet = "for ${1:i} = ${2:1}, ${3:10} do\n  ${4:-- body}\nend",
				},
			},
			go = {
				{
					trig = "func",
					desc = "Function definition",
					snippet = "func ${1:FunctionName}(${2:args}) ${3:returnType} {\n  ${4:// body}\n}",
				},
				{
					trig = "forloop",
					desc = "For loop",
					snippet = "for ${1:i} := ${2:0}; ${1:i} < ${3:10}; ${1:i}++ {\n  ${4:// body}\n}",
				},
				{
					trig = "if",
					desc = "If statement",
					snippet = "if ${1:condition} {\n  ${2:// body}\n}",
				},
			},
			python = {
				{
					trig = "def",
					desc = "Function definition",
					snippet = "def ${1:function_name}(${2:params}):\n  ${0:# TODO}",
				},
				{
					trig = "for",
					desc = "For loop",
					snippet = "for ${1:item} in ${2:iterable}:\n  ${0:# body}",
				},
				{
					trig = "if",
					desc = "If statement",
					snippet = "if ${1:condition}:\n  ${0:# body}",
				},
				{
					trig = "dic",
					desc = "Dictionary creation",
					snippet = "${1:my_dict} = {\n  '${2:key}': ${3:value},\n  ${0:# more items}\n}",
				},
				{
					trig = "la",
					desc = "List creation",
					snippet = "${1:my_list} = [${2:item1}, ${3:item2}, ${0:# more items}]",
				},
				{
					trig = "se",
					desc = "Set creation",
					snippet = "${1:my_set} = {${2:item1}, ${3:item2}, ${0:# more items}}",
				},
				{
					trig = "tu",
					desc = "Tuple creation",
					snippet = "${1:my_tuple} = (${2:item1}, ${3:item2}, ${0:# more items})",
				},
				{
					trig = "lc",
					desc = "List comprehension",
					snippet = "${1:my_list} = [${2:expression} for ${3:item} in ${4:iterable} if ${5:condition}]",
				},
				{
					trig = "dc",
					desc = "Dictionary comprehension",
					snippet = "${1:my_dict} = {${2:key}: ${3:value} for ${4:item} in ${5:iterable} if ${6:condition}}",
				},
			},

			javascript = {
				{
					trig = "function",
					desc = "Function definition",
					snippet = "function ${1:name}(${2:args}) {\n  ${3:// body}\n}",
				},
				{
					trig = "for",
					desc = "For loop",
					snippet = "for (let ${1:i} = ${2:0}; ${1:i} < ${3:10}; ${1:i}++) {\n  ${4:// body}\n}",
				},
				{
					trig = "if",
					desc = "If statement",
					snippet = "if (${1:condition}) {\n  ${2:// body}\n}",
				},
			},
			html = {
				{
					trig = "doctype",
					desc = "HTML5 Doctype",
					snippet = "<!DOCTYPE html>",
				},
				{
					trig = "html",
					desc = "HTML template",
					snippet = '<html lang="${1:en}">\n  <head>\n    <meta charset="UTF-8">\n    <title>${2:Title}</title>\n  </head>\n  <body>\n    ${3:Content}\n  </body>\n</html>',
				},
				{
					trig = "link",
					desc = "Anchor tag",
					snippet = '<a href="${1:url}">${2:text}</a>',
				},
			},
			sql = {
				{
					trig = "select",
					desc = "SELECT statement",
					snippet = "SELECT ${1:columns} FROM ${2:table} WHERE ${3:condition};",
				},
				{
					trig = "insert",
					desc = "INSERT INTO statement",
					snippet = "INSERT INTO ${1:table} (${2:columns}) VALUES (${3:values});",
				},
				{
					trig = "update",
					desc = "UPDATE statement",
					snippet = "UPDATE ${1:table} SET ${2:column} = ${3:value} WHERE ${4:condition};",
				},
			},
			toml = {
				{
					trig = "table",
					desc = "Table definition",
					snippet = "[${1:table_name}]",
				},
				{
					trig = "key",
					desc = "Key-value pair",
					snippet = '${1:key} = "${2:value}"',
				},
			},
			json = {
				{
					trig = "object",
					desc = "Object definition",
					snippet = '{\n  "${1:key}": "${2:value}"\n}',
				},
				{
					trig = "array",
					desc = "Array definition",
					snippet = "[\n  ${1:items}\n]",
				},
			},
			yaml = {
				{
					trig = "key_value",
					desc = "Key-value pair",
					snippet = "${1:key}: ${2:value}",
				},
				{
					trig = "list",
					desc = "List item",
					snippet = "- ${1:item}",
				},
			},
			yaml = {
				{
					trig = "key_value",
					desc = "Key-value pair",
					snippet = "${1:key}: ${2:value}",
				},
				{
					trig = "list",
					desc = "List item",
					snippet = "- ${1:item}",
				},
			},
			markdown = {
				{
					trig = "link",
					desc = "Internal Link",
					snippet = "[[${1:note_title}]]",
				},
				{
					trig = "img",
					desc = "Image embed",
					snippet = "![${1:alt_text}](${2:image_path})",
				},
				{
					trig = "codeblock",
					desc = "Code block",
					snippet = "``` ${1:language}\n${2:code}\n```",
				},
				{
					trig = "bold",
					desc = "Bold text",
					snippet = "**${1:bold_text}**",
				},
				{
					trig = "italic",
					desc = "Italic text",
					snippet = "*${1:italic_text}*",
				},
				{
					trig = "quote",
					desc = "Blockquote",
					snippet = "> ${1:quote_text}",
				},
				{
					trig = "task",
					desc = "Task checkbox",
					snippet = "- [ ] ${1:task_description}",
				},
				{
					trig = "tag",
					desc = "Tag",
					snippet = "#${1:tag}",
				},
				{
					trig = "header1",
					desc = "Header 1",
					snippet = "# ${1:Heading 1}",
				},
				{
					trig = "header2",
					desc = "Header 2",
					snippet = "## ${1:Heading 2}",
				},
				{
					trig = "header3",
					desc = "Header 3",
					snippet = "### ${1:Heading 3}",
				},
			},
		},
	},
}
